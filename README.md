# Sofia

Time zone: Australia/Sydney

Pronouns: she/her

## Accounts

IRC: Sofia on https://libera.chat and https://oftc.net.

Matrix: https://matrix.to/#/@sofia-snow:matrix.org

## PGP key for code signing

All code is signed using this signing-only key. You cannot encrypt messages
against it.

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYOqI3xYJKwYBBAHaRw8BAQdAGuSn36wy1H9F8H9wPgB5Dv3XZYw5ko3pRsrw
xKdItq+0BVNvZmlhiJAEExYIADgWIQTSDyuQGJPagBz1HW4zaA2j6sseOQUCYOqI
3wIbAwULCQgHAgYVCgkICwIEFgIDAQIeAQIXgAAKCRAzaA2j6sseOcCOAP9RRWTe
DimwqHn9ElCfCsdSNk4InKEpiDVb6ol83U9cBgEA47gIffPvPRcbWwAAVN5+5uLq
q8Dt+zc+lNZF5/G14wA=
=fyXm
-----END PGP PUBLIC KEY BLOCK-----

Public-Key Packet, old CTB, 51 bytes
    Version: 4
    Creation time: 2021-07-11 05:59:59 UTC
    Pk algo: EdDSA Edwards-curve Digital Signature Algorithm
    Pk size: 256 bits
    Fingerprint: D20F 2B90 1893 DA80 1CF5  1D6E 3368 0DA3 EACB 1E39
    KeyID: 3368 0DA3 EACB 1E39

User ID Packet, old CTB, 5 bytes
    Value: Sofia

Signature Packet, old CTB, 144 bytes
    Version: 4
    Type: PositiveCertification
    Pk algo: EdDSA Edwards-curve Digital Signature Algorithm
    Hash algo: SHA256
    Hashed area:
      Issuer Fingerprint: D20F 2B90 1893 DA80 1CF5  1D6E 3368 0DA3 EACB 1E39
      Signature creation time: 2021-07-11 05:59:59 UTC
      Key flags: CS
      Symmetric algo preferences: AES256, AES192, AES128, TripleDES
      Hash preferences: SHA512, SHA384, SHA256, SHA224, SHA1
      Compression preferences: Zlib, BZip2, Zip
      Features: MDC
      Keyserver preferences: no modify
    Unhashed area:
      Issuer: 3368 0DA3 EACB 1E39
    Digest prefix: C08E
    Level: 0 (signature over data)
```
